package io;

import java.io.*;

public class IOManagerImpl implements IOManager {

    //for every copy method we will calculate time in milliseconds,
    //we will count how many times read is invoked,
    //and length for both files

    @Override
    public void copyFileByteByByte(File from, File to, boolean append) {
        this.validateFile(from);
        System.out.println("copyFileByteByByte");

        //before java 7
/*        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(from);
            os = new FileOutputStream(to, append); //append is file exists and we will continue writing without deleting old content
            int read;
            while ((read = is.read()) != -1) {
                readCounter++;
                os.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        //we have to free all the resources
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (os != null) {
                try {
                    os.flush(); //this is not mandatory because close is invoking flush automatically
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every byte
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes", to.length()));*/


        //after java 7, we can use try block, so all resources are automatically free after finally block
        //we don't need to close them manually like

        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (InputStream is = new FileInputStream(from);
             OutputStream os = new FileOutputStream(to, append)) {
            int read;
            while ((read = is.read()) != -1) {
                readCounter++;
                os.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every byte
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileWithCustomByteBuffer(File from, File to, boolean append) {
        this.validateFile(from);
        System.out.println("copyFileWithCustomByteBuffer");

        //using custom buffer to make the copy process much faster
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (InputStream is = new FileInputStream(from);
             OutputStream os = new FileOutputStream(to, append)) {
            int read;
            int offset = 0;
            int length = 512;
            byte[] buffer = new byte[length];
            while ((read = is.read(buffer, offset, length)) != -1) {
                readCounter++;
                os.write(buffer, offset, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration)); //much faster then before
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every 'buffer size' bytes
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileWithJavaByteBuffer(File from, File to, boolean append) {
        this.validateFile(from);
        System.out.println("copyFileWithJavaByteBuffer");
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (InputStream is = new FileInputStream(from);
             BufferedInputStream bis = new BufferedInputStream(is);
             OutputStream os = new FileOutputStream(to, append);
             BufferedOutputStream bos = new BufferedOutputStream(os)) {
            int read;
            while ((read = bis.read()) != -1) {
                readCounter++;
                bos.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every byte
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileCharByChar(File from, File to, boolean append) {
        this.validateFile(from);
        System.out.println("copyFileCharByChar");
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (FileReader fr = new FileReader(from);
             FileWriter fw = new FileWriter(to, append)) {
            int read;
            while ((read = fr.read()) != -1) {
                readCounter++;
                fw.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every char, we can see that read is ~800 times less invoked (that is because chinese characters are 2 bytes)
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileCharByCharBuffered(File from, File to, boolean append) {
        this.validateFile(from);
        System.out.println("copyFileCharByCharBuffered");
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (FileReader fr = new FileReader(from);
             BufferedReader br = new BufferedReader(fr);
             FileWriter fw = new FileWriter(to, append);
             BufferedWriter bw = new BufferedWriter(fw)) {
            int read;
            while ((read = br.read()) != -1) {
                readCounter++;
                bw.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every char, we can see that read is ~800 times less invoked (that is because chinese characters are 2 bytes)
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileLineByLine(File from, File to, boolean append) {
        //this method will fail if a line is too large (more memory then our main memory)
        this.validateFile(from);
        System.out.println("copyFileLineByLine");
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (FileReader fr = new FileReader(from);
             BufferedReader br = new BufferedReader(fr);
             FileWriter fw = new FileWriter(to, append);
             BufferedWriter bw = new BufferedWriter(fw)) {
            String line;
            while ((line = br.readLine()) != null) {
                readCounter++;
                bw.write(line);
                bw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every line
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileWithConvertingByteStreamToCharStreamWithCharset(File from, File to, boolean append, String charset) {
        this.validateFile(from);
        System.out.println("copyFileWithConvertingByteStreamToCharStreamWithCharset");
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(from), charset));
             BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(to), charset))) {
            int read;
            while ((read = br.read()) != -1) {
                readCounter++;
                bw.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every char
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void copyFileContentReversed(File from, File to) {
        this.validateFile(from);
        System.out.println("copyFileContentReversed");
        long startTime = System.currentTimeMillis();
        long readCounter = 0;
        try (RandomAccessFile raf = new RandomAccessFile(from, "r");
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(to))) {
            int read;
            for (long i = raf.length() - 1; i >= 0; i--) {
                readCounter++;
                raf.seek(i);
                read = raf.read();
                bos.write(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we don't have to close streams/readers/writers manually
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Copy duration: %d", duration));
        System.out.println(String.format("Number of times read is invoked: %d", readCounter)); //for every line
        System.out.println(String.format("Source file length: %d bytes", from.length()));
        System.out.println(String.format("Destination file length: %d bytes\n", to.length()));
    }

    @Override
    public void writeAndReadBytesAsPrimitives(File file) {
        this.validateFile(file);
        System.out.println("writeAndReadBytesAsPrimitives");
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
             DataInputStream dis = new DataInputStream(new FileInputStream(file))) {

            dos.writeDouble(123d);
            dos.writeUTF("Writing string");
            dos.writeBoolean(false);

            Double d = dis.readDouble();
            System.out.println(d);
            String s = dis.readUTF();
            System.out.println(s);
            boolean b = dis.readBoolean();
            System.out.println(b);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //use PrintWriter or PrintStream if you want to print more sophisticated
    //PrintWriter is Writer (char oriented)
    @Override
    public void printFileFormattedWithLineNumberCharOriented(File file, OutputStream os) {
        this.validateFile(file);
        System.out.println("printFileFormattedWithLineNumberCharOriented");
        try (BufferedReader br = new BufferedReader(new FileReader(file));
             PrintWriter pw = new PrintWriter(os)) {
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                pw.println(String.format("%d %s", counter++, line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //use PrintWriter or PrintStream if you want to print more sophisticated (using println, etc...)
    //PrintStream is InputStream (byte oriented)
    @Override
    public void printFileFormattedWithLineNumberByteOrientedReadingFromStream(InputStream is, OutputStream os) {
        System.out.println("printFileFormattedWithLineNumberByteOrientedReadingFromStream");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is));
             PrintStream ps = new PrintStream(os)) {
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                ps.println(String.format("%d %s", counter++, line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void validateFile(File file) {
        if (!file.exists()) {
            throw new FileDoesNotExistException();
        }
        if (file.isDirectory()) {
            throw new FileIsDirectoryException();
        }
    }
}

class FileDoesNotExistException extends RuntimeException {

}


class FileIsDirectoryException extends RuntimeException {

}


