package io;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public interface IOManager {
    void copyFileByteByByte(File from, File to, boolean append);

    void copyFileWithCustomByteBuffer(File from, File to, boolean append);

    void copyFileWithJavaByteBuffer(File from, File to, boolean append);

    void copyFileCharByChar(File from, File to, boolean append);

    void copyFileCharByCharBuffered(File from, File to, boolean append);

    void copyFileLineByLine(File from, File to, boolean append);

    void copyFileWithConvertingByteStreamToCharStreamWithCharset(File from, File to, boolean append, String charset);

    void copyFileContentReversed(File from, File to);

    void writeAndReadBytesAsPrimitives(File filePath);

    void printFileFormattedWithLineNumberCharOriented(File filePath, OutputStream os);

    void printFileFormattedWithLineNumberByteOrientedReadingFromStream(InputStream is, OutputStream os);
}
