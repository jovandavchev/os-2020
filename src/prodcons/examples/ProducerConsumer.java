package prodcons.examples;

import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

import prodcons.*;

//dva nachini na reshavanje na problemot
public class ProducerConsumer {

    // TODO: definirajte gi semaforite i ostanatite promenlivi. Mora da se static

    //reshenie 1
//    public static Semaphore bufferEmpty;
//    public static Semaphore bufferLock;
//    public static Semaphore []items;

    //reshenie 2
    public static Semaphore bufferEmpty;
    public static Semaphore bufferLock;
    public static Semaphore items;
    public static Semaphore consumers;

    /**
     * Metod koj treba da gi inicijalizira vrednostite na semaforite i
     * ostanatite promenlivi za sinhronizacija.
     * <p>
     * TODO: da se implementira
     */
    public static void init() {
        //reshenie 1
//        bufferEmpty = new Semaphore(1);
//        bufferLock = new Semaphore(1);
//        items = new Semaphore[state.getBufferCapacity()];
//        for (int i = 0; i < state.getBufferCapacity(); i++) {
//            items[i] = new Semaphore(0);
//        }

        //reshenie 2
        bufferEmpty = new Semaphore(1);
        bufferLock = new Semaphore(1);
        items = new Semaphore(0);
        consumers = new Semaphore(0);
    }

    static class Producer extends TemplateThread {

        public Producer(int numRuns) {
            super(numRuns);
        }

        @Override
        public void execute() throws InterruptedException {
            //reshenie 1
//            bufferEmpty.acquire();
//            bufferLock.acquire();
//            state.fillBuffer();
//            for (Semaphore s : items) {
//                s.release();
//            }
//            bufferLock.release();

            //reshenie 2
            bufferEmpty.acquire();
            bufferLock.acquire();
            state.fillBuffer();
            bufferLock.release();
            items.release(state.getBufferCapacity());
        }
    }

    static class Consumer extends TemplateThread {
        private int cId;

        public Consumer(int numRuns, int id) {
            super(numRuns);
            cId = id;
        }

        @Override
        public void execute() throws InterruptedException {

            //reshenie 1
//            items[cId].acquire();
//            state.getItem(cId);
//
//            bufferLock.acquire();
//            state.decrementNumberOfItemsLeft();
//            if (state.isBufferEmpty()) {
//                bufferEmpty.release();
//            }
//            bufferLock.release();


            //reshenie 2
            items.acquire();
            state.getItem(cId);

            bufferLock.acquire();
            state.decrementNumberOfItemsLeft();
            if (state.isBufferEmpty()) {
                bufferEmpty.release();
                consumers.release(state.getBufferCapacity());
            }
            bufferLock.release();
            consumers.acquire();
        }
    }

    static State state;

    static class State extends AbstractState {

        private static final String _10_DVAJCA_ISTOVREMENO_PROVERUVAAT = "Dvajca istovremeno proveruvaat dali baferot e prazen. Maksimum eden e dozvoleno.";
        private static final String _10_KONZUMIRANJETO_NE_E_PARALELIZIRANO = "Konzumiranjeto ne e paralelizirano.";
        private int bufferCapacity = 15;

        private BoundCounterWithRaceConditionCheck[] items;
        private BoundCounterWithRaceConditionCheck counter = new BoundCounterWithRaceConditionCheck(
                0);
        private BoundCounterWithRaceConditionCheck raceConditionTester = new BoundCounterWithRaceConditionCheck(
                0);
        private BoundCounterWithRaceConditionCheck bufferFillCheck = new BoundCounterWithRaceConditionCheck(
                0, 1, 10, "", null, 0, null);

        public int getBufferCapacity() {
            return bufferCapacity;
        }

        private int itemsLeft = 0;

        public State(int capacity) {
            bufferCapacity = capacity;
            items = new BoundCounterWithRaceConditionCheck[bufferCapacity];
            for (int i = 0; i < bufferCapacity; i++) {
                items[i] = new BoundCounterWithRaceConditionCheck(0, null, 0,
                        null, 0, 10, "Ne moze da se zeme od prazen bafer.");
            }
        }

        public boolean isBufferEmpty() throws RuntimeException {
            log(raceConditionTester.incrementWithMax(), "checking buffer state");
            boolean empty = false;
            synchronized (this) {
                empty = (itemsLeft == 0);
            }
            log(raceConditionTester.decrementWithMin(), null);
            return empty;
        }

        public void getItem(int index) {
            counter.incrementWithMax(false);
            log(items[index].decrementWithMin(), "geting item");
            counter.decrementWithMin(false);
        }

        public void decrementNumberOfItemsLeft() {
            counter.incrementWithMax(false);
            synchronized (this) {
                itemsLeft--;
            }
            counter.decrementWithMin(false);
        }

        public void fillBuffer() {
            log(bufferFillCheck.incrementWithMax(), "filling buffer");
            if (isBufferEmpty()) {
                for (int i = 0; i < bufferCapacity; i++) {
                    items[i].incrementWithMax();

                }
            } else {
                logException(new PointsException(10, "Filling non-empty buffer"));
            }
            synchronized (this) {
                itemsLeft = bufferCapacity;
            }
            log(bufferFillCheck.decrementWithMin(), null);
        }

        public void finalize() {
            if (counter.getMax() == 1) {
                logException(new PointsException(10,
                        _10_KONZUMIRANJETO_NE_E_PARALELIZIRANO));
            }
        }
    }

    public static void main(String[] args) {
        try {
            Scanner s = new Scanner(System.in);
            int brKonzumeri = s.nextInt();
            int numIterations = s.nextInt();
            s.close();

            HashSet<Thread> threads = new HashSet<>();

            for (int i = 0; i < brKonzumeri; i++) {
                Consumer c = new Consumer(numIterations, i);
                threads.add(c);
            }
            Producer p = new Producer(numIterations);
            threads.add(p);

            state = new State(brKonzumeri);

            init();

            ProblemExecution.start(threads, state);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // </editor-fold>
}
