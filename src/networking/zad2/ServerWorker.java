package networking.zad2;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class ServerWorker extends Thread {
    Socket socket;
    Server server;

    public ServerWorker(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;
    }


    @Override
    public void run() {
        DataInputStream input = null;
        DataOutputStream output = null;
        try {

            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());

            String line;

            //dobivame poraka od klientot vo format Hello from: client1
            line = input.readUTF();
            System.out.println(line);

            //na klientot mu vrakjame lista od kandidati
            output.writeUTF(server.getListOfCandidates());


            //klientot ni prakja brojka za koj kandidat kje glasa
            int candidateNumber = input.readInt();
            this.server.vote(socket.getRemoteSocketAddress(), candidateNumber);


            //prashaj go klientot dali saka da gi vidi momentalnite rezultati
            output.writeUTF("Dali sakate da gi vidite rezultatite? (DA/NE)");


            line = input.readUTF();

            if ("DA".equals(line)) {
                //if DA -> rezultati
                output.writeUTF(server.getRezultati());
            } else {
                //if NE -> Prijatno
                output.writeUTF("Prijatno");
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            server.turnOffConnection();
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


}
