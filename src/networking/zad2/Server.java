package networking.zad2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

public class Server extends Thread {

    public Semaphore threadPool = new Semaphore(100);
    Map<Integer, String> candidates;
    Map<Integer, Integer> votes;
    int port;
    ServerSocket server = null;
    boolean active;
    PrintWriter writeToLog;
    private Semaphore voteLock;


    public Server(int port, List<String> list, String path) {
        this.port = port;
        this.candidates = new TreeMap<>();
        this.votes = new TreeMap<>();
        this.active = true;
        int counter = 1;
        for (String c : list) {
            this.candidates.put(counter, c);
            this.votes.put(counter++, 0);
        }
        try {
            this.writeToLog = new PrintWriter(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.voteLock = new Semaphore(1);
    }


    @Override
    public void run() {
        try {
            this.server = new ServerSocket(this.port);
            System.out.println("Serverot e aktiven");

            while (active) {
                threadPool.acquire();
                Socket socket = this.server.accept();
                ServerWorker serverWorker = new ServerWorker(this, socket);
                serverWorker.start();
            }
            System.out.println("Serverot ne e aktiven povekje");

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void turnOffServer() {
        this.active = false;
    }

    public void turnOffConnection() {
        threadPool.release();
    }

    public String getListOfCandidates() {
        return this.candidates.entrySet()
                .stream()
                .map(entry -> String.format("%d - %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining("\n"));
    }

    public void vote(SocketAddress remoteSocketAddress, int candidateNumber) {
        try {
            this.voteLock.acquire();
            String message = String.format("%s - %d", remoteSocketAddress, candidateNumber);
            Integer votes = this.votes.get(candidateNumber);
            votes++;
            this.votes.put(candidateNumber, votes);
            writeToLog.println(message);
            writeToLog.flush();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            this.voteLock.release();
        }
    }

    public String getRezultati() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : this.votes.entrySet()) {
            String name = this.candidates.get(entry.getKey());
            sb.append(String.format("%d - %s - brGlasovi: %d", entry.getKey(), name, entry.getValue()));
            sb.append("\n");
        }
        sb.append("\n");
        return sb.toString();
    }
}
