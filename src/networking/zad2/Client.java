package networking.zad2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client extends Thread {
    private String name;
    String host;
    int port;


    public Client(String name, String host, int port) {
        this.name = name;
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        DataInputStream input = null;
        DataOutputStream output = null;
        try {
            Socket socket = new Socket(host, port);
            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());

            //prakjame poraka do serverot vo format Hello from: client1
            output.writeUTF(String.format("Hello from: %s", this.name));

            //od serverot dobivame lista od kandidati
            String lista = input.readUTF();
            print(lista);


            //prakjame brojka za kanditatot koj glasame
            Scanner scanner = new Scanner(System.in);
            int candidateNumber = scanner.nextInt();
            output.writeInt(candidateNumber);


            //serverot ne prashuva dali sakame da gi vidime momentalnite rezultati
            String message = input.readUTF();
            print(message);


            //odgovarame Da ili NE
            String answer = scanner.next();
            output.writeUTF(answer);


            //serverot ni prakja odgovor
            message = input.readUTF();
            print(message);

            print("Klientot zavrshi!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void print(String message) {
        System.out.println(String.format("%s - %s",this.name, message));
    }
}
