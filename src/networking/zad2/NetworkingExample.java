package networking.zad2;

import java.util.ArrayList;
import java.util.List;

public class NetworkingExample {
    public static void main(String[] args) throws InterruptedException {
        int port = 9879;
        String k1 = "Zoran Zaev";
        String k2 = "Hristijan Mickovski";
        String k3 = "Ali Ahmeti";
        List<String> lista = new ArrayList<>();
        lista.add(k1);
        lista.add(k2);
        lista.add(k3);
        Server server = new Server(port, lista, "votes.txt");
        server.start();
        Thread.sleep(1000);

        Client c1 = new Client("client1", "localhost", port);
        c1.start();
        Client c2 = new Client("client2", "localhost", port);
        c2.start();
        Client c3 = new Client("client3", "localhost", port);
        c3.start();
        Client c4 = new Client("client4", "localhost", port);
        c4.start();

    }
}
