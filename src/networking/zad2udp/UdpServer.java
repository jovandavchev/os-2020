package networking.zad2udp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

public class UdpServer extends Thread {

    int port;
    DatagramSocket server;
    Map<Integer, String> candidates;
    Map<Integer, Integer> votes;
    boolean active;

    PrintWriter writeToLog;

    HashSet<SocketAddress> helloAddresses;
    HashSet<SocketAddress> alreadyVoted;


    public UdpServer(int port, List<String> list, String path) {
        this.port = port;
        this.candidates = new TreeMap<>();
        this.votes = new TreeMap<>();
        int counter = 1;
        for (String c : list) {
            this.candidates.put(counter, c);
            this.votes.put(counter++, 0);
        }
        this.active = true;
        this.helloAddresses = new HashSet<>();
        this.alreadyVoted = new HashSet<>();
        try {
            this.writeToLog = new PrintWriter(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            server = new DatagramSocket(port);

            while (active) {
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

                server.receive(packet);
                String message = new String(packet.getData(), 0, packet.getLength());
                int port = packet.getPort();
                InetAddress address = packet.getAddress();
                SocketAddress socketAddress = packet.getSocketAddress();

                Integer candidateNumber = -1;
                try {
                    candidateNumber = Integer.parseInt(message);
                } catch (NumberFormatException ex) {
                    candidateNumber = -1;
                }

                boolean flag = true;

                if (message.contains("Hello from:")) {
//                    String client = message.split(":")[1].trim();
                    buffer = this.getListOfCandidates().getBytes();
                    helloAddresses.add(socketAddress);
                } else if (candidateNumber > 0) {
                    if (helloAddresses.contains(socketAddress)) {
//                        helloAddresses.remove(socketAddress);
                        this.vote(socketAddress, candidateNumber);
                        this.alreadyVoted.add(socketAddress);
                    }
                    buffer = "Dali sakate da gi vidite rezultatite? (DA/NE)".getBytes();
                } else if (message.equals("DA")) {
                    if (alreadyVoted.contains(socketAddress)) {
//                        alreadyVoted.remove(socketAddress);
                        buffer = this.getRezultati().getBytes();
                    }
                } else if (message.equals("NE")) {
                    buffer = "Prijatno!".getBytes();
                } else {
                    flag = false;
                }


//                DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length, socketAddress);
                if (flag) {
                    DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length, address, port);
                    server.send(sendPacket);
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getListOfCandidates() {
        return this.candidates.entrySet()
                .stream()
                .map(entry -> String.format("%d - %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining("\n"));
    }

    public void vote(SocketAddress remoteSocketAddress, int candidateNumber) {
        String message = String.format("%s - %d", remoteSocketAddress, candidateNumber);
        Integer votes = this.votes.get(candidateNumber);
        votes++;
        this.votes.put(candidateNumber, votes);
        writeToLog.println(message);
        writeToLog.flush();
    }

    public String getRezultati() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : this.votes.entrySet()) {
            String name = this.candidates.get(entry.getKey());
            sb.append(String.format("%d - %s - brGlasovi: %d", entry.getKey(), name, entry.getValue()));
            sb.append("\n");
        }
        sb.append("\n");
        return sb.toString();
    }
}
