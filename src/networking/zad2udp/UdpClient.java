package networking.zad2udp;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class UdpClient extends Thread {

    private String name;
    String host;
    int port;

    public UdpClient(String name, String host, int port) {
        this.name = name;
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket();
            byte [] buffer = new byte[1024];


            //prakjame poraka do serverot vo format Hello from: client1
            String message = String.format("Hello from: %s", this.name);
            buffer = message.getBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(host), port);
            socket.send(packet);


            //od serverot dobivame lista od kandidati
            buffer = new byte[1024];
            packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            message = new String(packet.getData(), 0, packet.getLength());
            print(message);


            //prakjame brojka za kanditatot koj glasame
            print("Vnesi broj na kandidat");
            Scanner scanner = new Scanner(System.in);
            int candidateNumber = scanner.nextInt();
            buffer = String.valueOf(candidateNumber).getBytes();
            packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(host), port);
            socket.send(packet);


            //serverot ne prashuva dali sakame da gi vidime momentalnite rezultati
            buffer = new byte[1024];
            packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            message = new String(packet.getData(), 0, packet.getLength());
            print(message);


            //odgovarame Da ili NE
            print("Odgovori DA ili NE");
            String answer = scanner.next();
            buffer = answer.getBytes();
            packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(host), port);
            socket.send(packet);


            //serverot ni prakja odgovor
            buffer = new byte[1024];
            packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            message = new String(packet.getData(), 0, packet.getLength());
            print(message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void print(String message) {
        System.out.println(String.format("%s - %s",this.name, message));
    }

}
