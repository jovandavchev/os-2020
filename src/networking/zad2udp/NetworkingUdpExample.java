package networking.zad2udp;

import networking.zad2.Client;
import networking.zad2.Server;

import java.util.ArrayList;
import java.util.List;

public class NetworkingUdpExample {

    public static void main(String[] args) throws InterruptedException {
        int port = 9879;
        String k1 = "Zoran Zaev";
        String k2 = "Hristijan Mickovski";
        String k3 = "Ali Ahmeti";


        List<String> lista = new ArrayList<>();
        lista.add(k1);
        lista.add(k2);
        lista.add(k3);
        UdpServer server = new UdpServer(port, lista, "votes-udp.txt");
        server.start();
        Thread.sleep(1000);

        UdpClient c1 = new UdpClient("client1", "localhost", port);
        c1.start();
//        UdpClient c2 = new UdpClient("client2", "localhost", port);
//        c2.start();
//        UdpClient c3 = new UdpClient("client3", "localhost", port);
//        c3.start();
//        UdpClient c4 = new UdpClient("client4", "localhost", port);
//        c4.start();
    }
}
