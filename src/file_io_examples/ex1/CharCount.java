package file_io_examples.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class CharCount {

    public static void main(String[] args) throws IOException {

        List<byte[]> bytes = CharCount.read("./src/file_io_examples/ex1/charcount.txt");

        for (byte[] array : bytes) {
            System.out.print("[ ");
            for (byte elem : array) {
                System.out.print(String.format("%d ", elem - '0'));
            }
            System.out.println("]");
        }
    }

    private static List<byte[]> read(String filePath) {
        List<byte[]> bytes = new ArrayList<>();
        try (InputStream is = new FileInputStream(filePath)) {
            int read;
            while ((read = is.read()) != -1) {
                int number = read - '0';

                byte[] array = new byte[number];
                for (int i = 0; i < number; i++) {
                    int value = is.read();
                    if (value == -1) {
                        System.out.println("This should not happen");
                        throw new IllegalStateException();
                    }
                    array[i] = (byte) value;
                }

                bytes.add(array);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

}
