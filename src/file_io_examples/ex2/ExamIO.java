package file_io_examples.ex2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ExamIO {

    public static void main(String[] args) throws IOException {

        ExamIO obj = new ExamIO();
//
//        obj.copyLargeTxtFiles("nepostoi", "asdf", 2); //treba da vrati deka ne postoi
//        obj.copyLargeTxtFiles("dat2.txt", "copiedFolder", 2); //treba da vrati deka ne e direktorium
//        obj.copyLargeTxtFiles("folder", "copiedFolder", 2); //treba uspeshno da go iskopira rekurzivno folderot

        List<byte[]> bytes = new ArrayList<>();
        bytes.add("12".getBytes());
        bytes.add("33".getBytes());
        bytes.add("56".getBytes());

        obj.serializeData("serialized_data.txt", bytes);

        byte[] res = obj.deserializeDataAtPosition("serialized_data.txt", 1, 2);
        System.out.println(new String(res));

    }


    private void copyLargeTxtFiles(String from, String to, long size) {
        File fromDirectory = new File(from);
        File toDirectory = new File(to);

        if (!fromDirectory.exists()) {
            System.out.println("Ne postoi");
            return;
        }
        if (!fromDirectory.isDirectory()) {
            System.out.println("Ne e direktorium");
            return;
        }

        if (toDirectory.exists()) {
            if (!toDirectory.isDirectory()) {
                System.out.println("Ne e direktorium");
                return;
            }
        } else {
            boolean created = toDirectory.mkdirs();
            if (!created) {
                System.out.println("Ne mozhe da se kreira direktoriumot");
                return;
            }
        }

        copyFiles(fromDirectory, toDirectory, size);

    }

    private void copyFiles(File fromDirectory, File toDirectory, long size) {
        File[] files = fromDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                File child = new File(file, s);
                if (child.isDirectory()) {
                    File toChildDirectory = new File(toDirectory, s);
                    if (!toChildDirectory.exists()) {
                        toChildDirectory.mkdirs();
                    }
                    copyFiles(child, toChildDirectory, size);
                    return false;
                } else {
                    return child.isFile() && s.endsWith(".txt") && child.length() > size;
                }
            }
        });

        assert files != null;
        //copy content
        for (File f : files) {
            try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f));
                 BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(toDirectory, f.getName())))) {
                int read;
                while ((read = bis.read()) != -1) {
                    bos.write(read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    void serializeData(String destination, List<byte[]> data) {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destination))) {
            for (byte[] elem : data) {
                for (byte b : elem) {
                    bos.write(b);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    byte[] deserializeDataAtPosition(String source, long position, long elementLength){
        byte[] array = new byte[(int) elementLength];
        try(RandomAccessFile raf = new RandomAccessFile(source, "r")) {
            raf.seek(position*elementLength);
            for (int i = 0; i < elementLength; i++) {
                int read = raf.read();
                if (read == -1) {
                    System.out.println("Greshka");
                    throw new IllegalStateException();
                }
                array[i] = (byte) read;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }
}
