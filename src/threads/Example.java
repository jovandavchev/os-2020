package threads;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


//kod od konsultacii
class Locks {
    public static Semaphore lock = new Semaphore(1);
    public static Semaphore bLock = new Semaphore(0);
}


class A extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                Locks.lock.acquire();
                System.out.println("A");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                Locks.bLock.release();
            }
        }
    }
}

class B extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                Locks.bLock.acquire();
                System.out.println("B");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                Locks.lock.release();
            }
        }
    }
}

//class Locks {
//    public static Semaphore lock = new Semaphore(1);
//}

class Result {
    long result;
//    Object lock;
//    private static Lock lock2 = new ReentrantLock();
//    private static Semaphore lockSemaphore = new Semaphore(1);


    public Result() {
        this.result = 0L;
    }

    public void addResult(long localResult)  {
//        lock2.lock();
//        this.result += localResult;
//        lock2.unlock();
//
//        synchronized (lock) {
//            this.result += localResult;
//        }

//        try {
//            Locks.lock.acquire();
//            this.result += localResult;
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } finally {
//            Locks.lock.release();
//        }

        this.result += localResult;
    }

    public Long getValue() {
        return this.result;
    }
}

class Counter extends Thread {

    int from, to;
    Result result;

    private static Semaphore localLock  = new Semaphore(1);

    public Counter(int from, int to, Result result) {
        this.from = from;
        this.to = to;
        this.result = result;
//        this.localLock =
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            try {
                Locks.lock.acquire();
                this.result.addResult(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                Locks.lock.release();
            }
        }
    }
}


public class Example {

    public static void main(String[] args) throws InterruptedException {
//        ThreadExample r1 = new ThreadExample("Thread1");
//        ThreadExample r2 = new ThreadExample("Thread2");
//        Thread t1 = new Thread(r1);
//        Thread t2 = new Thread(r2);
//        t1.start();
//        t2.start();
//        t1.join();
//        for (int i = 0; i < 1000; i++) {
//            System.out.println("Main " + i);
//        }

        Result result = new Result();
        Counter c1 = new Counter(1, 250000, result);
        Counter c2 = new Counter(250001, 500000, result);
        c1.start();
        c2.start();
        c1.join();
        c2.join();
        System.out.println(result.getValue());

//        A a = new A();
//        B b = new B();
//        a.start();
//        b.start();
    }
}


class ThreadExample extends BaseClass implements Runnable {

    private String threadName;

    public ThreadExample(String name) {
        this.threadName = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(String.format("%s : %d", this.threadName, i));
        }
    }
}

class BaseClass {

}