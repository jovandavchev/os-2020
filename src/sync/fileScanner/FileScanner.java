package sync.fileScanner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FileScanner extends Thread  {

    private String fileToScan;
    //TODO: Initialize the start value of the counter
    private static Long counter = 0L;
    private static Semaphore counterLock = new Semaphore(1);

    public FileScanner (String fileToScan) {
        this.fileToScan=fileToScan;
        //TODO: Increment the counter on every creation of FileScanner object
        synchronized (FileScanner.class) {
            counter++;
        }

//        try {
//            counterLock.acquire();
//            counter++;
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } finally {
//            counterLock.release();
//        }
    }

    public static void printInfo(File file)  {

        /*
         * TODO: Print the info for the @argument File file, according to the requirement of the task
         * */
        if (file.isFile()) {
            System.out.println("file: " + file.getAbsolutePath() + " " + file.length());
        } else {
            System.out.println("dir: " + file.getAbsolutePath() + " " + file.length());
        }
    }

    public static Long getCounter () {
        return counter;
    }


    public void run() {

        //TODO Create object File with the absolute path fileToScan.
        File file = new File(this.fileToScan);

        List<FileScanner> scanners = new ArrayList<>();

        //TODO Create a list of all the files that are in the directory file.
        if (!file.isDirectory()) {
            throw new RuntimeException("Ne e direktorium");
        }
        File [] files = file.listFiles();

        for (File f : files) {
            printInfo(f);
            if (f.isDirectory()) {
                String[] childNames = f.list();
                if (childNames != null && childNames.length > 0) {
                    FileScanner fs = new FileScanner(f.getAbsolutePath());
                    fs.start();
                    scanners.add(fs);
                }
            }
        }


        for (FileScanner f : scanners) {
            try {
                f.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//
//        for (File f : files) {
//
//
//        }

    }

    public static void main (String [] args) {
        String FILE_TO_SCAN = "./copiedFolder";

        //TODO Construct a FileScanner object with the fileToScan = FILE_TO_SCAN
        FileScanner fileScanner = new FileScanner(FILE_TO_SCAN);

        //TODO Start the thread from type FileScanner
        fileScanner.start();

        //TODO wait for the fileScanner to finish
        try {
            fileScanner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //TODO print a message that displays the number of thread that were created
        System.out.println(getCounter());

    }
}