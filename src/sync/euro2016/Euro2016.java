package sync.euro2016;

import java.util.HashSet;
import java.util.concurrent.Semaphore;

public class Euro2016 {

    //dek
    static Semaphore lock;

    static int fanACount;
    static int fanBCount;

    static Semaphore fanAReady;
    static Semaphore fanBReady;

    static Semaphore finished;

//    /init
    public static void init() {
        lock = new Semaphore(1);
        fanACount = 0;
        fanBCount = 0;
        fanAReady = new Semaphore(0);
        fanBReady = new Semaphore(0);
        finished = new Semaphore(0);
    }


    public static State state = new State();

    static class State {

        public void board(String from) {
            System.out.println("Board from: " + from);
        }

        public void departure() {
            System.out.println("Departure");
        }

    }

    static class FanA extends Thread {
        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void execute() throws InterruptedException {

            boolean controller = false;

            lock.acquire();
            fanACount++;

            if (fanACount == 4) {
                controller = true;
                fanACount -= 4;
                fanAReady.release(4);
            } else if (fanACount == 2 && fanBCount >= 2) {
                controller = true;
                fanACount -= 2;
                fanBCount -= 2;
                fanAReady.release(2);
                fanBReady.release(2);
            } else {
                lock.release();
            }

            fanAReady.acquire();
            state.board("FanA");
            finished.release();

            if (controller) {
                finished.acquire(4);
                state.departure();
                lock.release();
            }

        }
    }

    static class FanB extends Thread {
        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void execute() throws InterruptedException {
            boolean controller = false;

            lock.acquire();
            fanBCount++;

            if (fanBCount== 4) {
                controller = true;
                fanBCount -= 4;
                fanBReady.release(4);
            } else if (fanBCount == 2 && fanACount >= 2) {
                controller = true;
                fanBCount -= 2;
                fanACount -= 2;
                fanAReady.release(2);
                fanBReady.release(2);
            } else {
                lock.release();
            }

            fanBReady.acquire();
            state.board("FanB");
            finished.release();

            if (controller) {
                finished.acquire(4);
                state.departure();
                lock.release();
            }
        }
    }


    public static void main(String[] args) throws InterruptedException {
        HashSet<Thread> threads = new HashSet<>();
        init();
        for (int i = 0; i < 120; i++) {
            threads.add(new FanA());
            threads.add(new FanB());
        }

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t: threads) {
            t.join(2000);
        }
    }
}
