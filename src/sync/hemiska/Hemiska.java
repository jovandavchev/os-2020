package sync.hemiska;

import java.util.HashSet;
import java.util.concurrent.Semaphore;

public class Hemiska {

    //
    static Semaphore kislorod;
    static Semaphore vodorod;
    static Semaphore jaglerod;

    static Semaphore jaglerodHere;
    static Semaphore vodorodHere;

    static Semaphore ready;
    static Semaphore finished;

    static int brojKislorod;
    static Semaphore lock;


    static State state = new State();

    static void init() {

        kislorod = new Semaphore(4);
        vodorod = new Semaphore(2);
        jaglerod = new Semaphore(2);

        jaglerodHere = new Semaphore(0);
        vodorodHere = new Semaphore(0);

        ready = new Semaphore(0);
        finished = new Semaphore(0);

        brojKislorod = 0;
        lock = new Semaphore(1);

    }

    static class Kislorod extends Thread {

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void execute() throws InterruptedException {
            kislorod.acquire();

            lock.acquire();
            brojKislorod++;
            if (brojKislorod == 4) {
                vodorodHere.acquire(2);
                jaglerodHere.acquire(2);
                ready.release(8);
            }
            lock.release();
            ready.acquire();
            state.bond("Kislorod");

            finished.release();

            lock.acquire();
            brojKislorod--;
            if (brojKislorod == 0) {
                finished.acquire(8);
                state.validate();
                kislorod.release(4);
                jaglerod.release(2);
                vodorod.release(2);
            }
            lock.release();

        }
    }

    static class Jaglerod extends Thread {
        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void execute() throws InterruptedException {
            jaglerod.acquire();
            jaglerodHere.release();
            ready.acquire();
            state.bond("Jaglerod");
            finished.release();
        }
    }

    static class Vodorod extends Thread {

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void execute() throws InterruptedException {
            vodorod.acquire();
            vodorodHere.release();
            ready.acquire();
            state.bond("Vodorod");
            finished.release();
        }

    }

    static class State {

        public void bond(String type) {
            System.out.println("Bond from " + type);
        }

        public void validate() {
            System.out.println("Validate");
        }
    }

    public static void main(String[] args) {
        HashSet<Thread> threads = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            threads.add(new Kislorod());
            threads.add(new Kislorod());
            threads.add(new Vodorod());
            threads.add(new Jaglerod());
        }

        init();

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            try {
                t.join(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
