package sync;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.Semaphore;

//kodirano za vreme na chas
//alternativno reshenie ima vo examples ProducerConsumer
//reshenieto e tochno, no ovde ne pravam dopolnitelni proverki za dali e uspeshna sinhronizacijata
class Locks {
    public static Semaphore bufferEmpty;
    public static Semaphore bufferLock;
    public static Semaphore items[];
}

public class ProdConsTester {
    public static final int NUM_RUNS = 30;

    public static void main(String[] args) {
        int numberOfConsumers = 120;
        MyState state = new MyState(numberOfConsumers);
        init(numberOfConsumers);
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < numberOfConsumers; i++) {
            Consumer consumer = new Consumer(state, i);
            threads.add(consumer);
        }
        Producer producer = new Producer(state);
        threads.add(producer);

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Uspeshna sinhronizacija");
    }

    public static void init(int numCons) {
        Locks.bufferLock = new Semaphore(1);
        Locks.bufferEmpty = new Semaphore(1);
        Locks.items = new Semaphore[numCons];
        for (int i = 0; i < numCons; i++) {
            Locks.items[i] = new Semaphore(0);
        }
    }
}


class Consumer extends Thread {

    MyState state;
    int id;

    public Consumer(MyState state, int id) {
        this.state = state;
        this.id = id;
    }

    @Override
    public void run() {
        for (int i = 0; i < ProdConsTester.NUM_RUNS; i++) {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void execute() throws InterruptedException {

        //zakluchi go delot za mojot item
        Locks.items[id].acquire();
        state.getItemById(id);

        //zakluchi go bafferot
        Locks.bufferLock.acquire();
        state.decrementBuffer();
        //proveri dali e prazen
        if (state.isBufferEmpty()) {
            //ako e prazen izvesti go producerot
            Locks.bufferEmpty.release();
        }

        Locks.bufferLock.release();
        //otkluchi go bafferot
    }
}

class Producer extends Thread {

    MyState state;

    public Producer(MyState state) {
        this.state = state;
    }

    @Override
    public void run() {
        for (int i = 0; i < ProdConsTester.NUM_RUNS; i++) {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void execute() throws InterruptedException {
        //proveri dali e prazen buffer-ot
        Locks.bufferEmpty.acquire();

        //zakluchi go bafferot
        Locks.bufferLock.acquire();
        state.fillBuffer();
        //izvesti gi konzumerite deka e poln baferot
        for (Semaphore s: Locks.items) {
            s.release();
        }
        Locks.bufferLock.release();
        //otkluchi go baferot

    }

}


class MyState {
    int items;
    int numberOfConsumers;

    public MyState(int numberOfConsumers) {
        this.items = 0;
        this.numberOfConsumers = numberOfConsumers;
    }

    public boolean isBufferEmpty() {
        return this.items == 0;
    }

    public int getNumberOfConsumers() {
        return numberOfConsumers;
    }

    public void fillBuffer() {
        if (this.items != 0) {
            System.out.println("GRESHKA");
            throw new RuntimeException("GRESHKA");
        }
        this.items = numberOfConsumers;
        System.out.println("FillBuffer");
    }

    public void decrementBuffer() {
        this.items--;
        if (this.items < 0) {
            System.out.println("GRESHKA");
            throw new RuntimeException("GRESHKA");
        }
        System.out.println("Decrement");
    }

    public void getItemById(int id) {
        System.out.println(String.format("Get item by id %d", id));
    }
}