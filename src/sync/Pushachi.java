package sync;

import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Pushachi {

    //ovde definirajte gi semaforite
    static Semaphore tableLock;
    static Semaphore []smokers;
    static Semaphore notify;

    static State state = new State();

    public static void init() {
        //ovde napravete init
        tableLock = new Semaphore(1);
        smokers = new Semaphore[3];
        for (int i = 0; i < 3; i++) {
            smokers[i] = new Semaphore(0);
        }
        notify = new Semaphore(0);
    }

    public static class Smoker extends Thread {

        int type;
        public Smoker(int type) {
            this.type = type;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    execute();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private void execute() throws InterruptedException {
            smokers[type].acquire();
            tableLock.acquire();
            if (state.hasMyItems(type)) {
                state.consume(type);
            }
            tableLock.release();
            notify.release();
        }
    }

    public static class Agent extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    execute();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private void execute() throws InterruptedException {
            tableLock.acquire();
            state.putItems();
            for (int i = 0; i < 3; i++) {
                smokers[i].release();
            }
            tableLock.release();
            notify.acquire(3);
        }
    }

    public static class State {
        boolean[] tableItems;

        public State() {
            tableItems = new boolean[3];
        }


        public void putItems() {
            int temp = new Random().nextInt(3);
            int a = (temp + 1) % 3;
            int b = (temp + 3 - 1) % 3;
            System.out.println("Putting items for types: " + a + " and " + b);
            tableItems[a] = true;
            tableItems[b] = true;
        }

        public boolean hasMyItems(int type) {
            int a = (type + 1) % 3;
            int b = (type + 3 - 1) % 3;
            return tableItems[a] && tableItems[b];
        }

        public void consume(int type) {
            System.out.println("Consuming items. Smoker type: " + type);
            tableItems = new boolean[3];
        }

    }

    public static void main(String[] args) {
        HashSet<Thread> threads = new HashSet<>();
        for (int i = 0; i < 3; i++) {
            threads.add(new Smoker(i));
        }
        threads.add(new Agent());
        init();
        for (Thread t : threads) {
            t.start();
        }

        for (Thread t: threads) {
            try {
                t.join(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


